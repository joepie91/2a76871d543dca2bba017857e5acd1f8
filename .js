var Promise = require("bluebird");

function lineStreamToPromise(stream) {
  return new Promise(function(resolve, reject) {
    var results = [];
    
    stream.on("data", function(object) {
      results.push(object);
    });
    
    stream.on("end", function() {
      resolve(results);
    })
    
    stream.on("error", function(err) {
      reject(err);
    })
  })
}